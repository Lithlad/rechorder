package com.chaber.rechorder

import com.vaadin.flow.spring.annotation.EnableVaadin
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan

// @EnableMongoRepositories(basePackages = ["song.repository.mongo"])
@EnableVaadin("song")
@SpringBootApplication
@ComponentScan(basePackages = ["song"])
open class RechorderApplication

fun main(args: Array<String>) {
    runApplication<RechorderApplication>(*args)
}
