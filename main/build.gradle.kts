plugins {
    id("org.unbroken-dome.test-sets") version "4.0.0" // to create additional test resources
}

testSets {
    val integrationTests by creating {
    }
}

// to integrate build with integration tests https://github.com/unbroken-dome/gradle-testsets-plugin
tasks.check {
    dependsOn(tasks.findByPath("integrationTests"))
}

dependencies {
    implementation(project(":core"))
    implementation(project(":rest"))
    implementation(project(":latexrepository"))
    implementation(project(":ui"))
    implementation("com.vaadin:vaadin-spring-boot-starter:${rootProject.properties["vaadinDependencyVersion"]}")
}

description = "main"
version = "0.0.1-SNAPSHOT"
group = "com.chaber"
