import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    // Versions are resolved in settings.gradle.kts
    id("org.springframework.boot")
    id("io.spring.dependency-management")
    // jacoco - test coverage reports
    id("jacoco")
    id("org.sonarqube")
    id("com.gorylenko.gradle-git-properties")
    id("org.jmailen.kotlinter")
    kotlin("jvm")
    kotlin("plugin.spring")
}

tasks.bootJar {
    enabled = false
}

tasks.jar {
    enabled = true
}

val gitProps get() = ext["gitProps"] as Map<String, String>
gitProperties {
    extProperty = "gitProps"
    keys = listOf(
        "git.branch",
        "git.build.version",
        "git.commit.id",
        "git.commit.id.abbrev",
        "git.commit.time",
        "git.tags",
        "git.remote.origin.url"
    )
}

val aggregatedTestReport = tasks.register<TestReport>("aggregatedTestReport") {
    destinationDir = file("$buildDir/reports/tests/test")
    reportOn(
        subprojects.mapNotNull {
            it.tasks.findByPath("test")
        }
    )
}

val aggregatedJacocoRootReport = tasks.register<JacocoReport>("aggregatedJacocoRootReport") {
    dependsOn(tasks.test)
    subprojects {
        this@subprojects.plugins.withType<JacocoPlugin>().configureEach {
            this@subprojects.tasks.matching {
                it.extensions.findByType<JacocoTaskExtension>() != null
            }.configureEach {
                sourceSets(this@subprojects.the<SourceSetContainer>().named("main").get())
                executionData(this)
            }
        }
    }

    reports {
        xml.required.set(true)
        csv.required.set(false)
    }
}

// https://docs.sonarqube.org/latest/analysis/scan/sonarscanner-for-gradle/
sonarqube {
    properties {
        property("sonar.host.url", "https://sonarcloud.io")
        property("sonar.projectKey", "Lithlad_rechorder")
        property("sonar.organization", "lithlad")
        // TODO get rid of sonar login in plain text
        property("sonar.login", "16605efc032817558b034e6a911ba37f6812bab1")
        property("sonar.language", "kotlin")
        property("sonar.issuesReport.html.enable", "true")
        property("sonar.coverage.jacoco.xmlReportPaths", "${project.rootDir}/build/reports/jacoco/aggregatedJacocoRootReport/aggregatedJacocoRootReport.xml")
        gitProps["git.branch"]?.let { property("sonar.branch", it) }
    }
}

// Left to remember about such possibility
// tasks["sonarqube"].mustRunAfter("generateGitProperties")
tasks["sonarqube"].dependsOn("generateGitProperties")

kotlinter {
    reporters = arrayOf("checkstyle", "plain")
    ignoreFailures = false
    experimentalRules = false
    disabledRules = emptyArray()
}

allprojects {
    // run plugins in each project
    apply(plugin = "org.jetbrains.kotlin.jvm")
    apply(plugin = "io.spring.dependency-management")
    apply(plugin = "jacoco")
    apply(plugin = "org.jmailen.kotlinter")
    // println("Hello from: ${project.group} ${project.name} ${project.version} ${project.description}")
    println("Found project: ${project.group} ${project.name}")

    group = "com.chaber"
    version = "0.0.1-SNAPSHOT"
    java.sourceCompatibility = JavaVersion.VERSION_11

    repositories {
        mavenCentral()
    }

    dependencies {
        implementation(kotlin("stdlib"))
        implementation("org.jetbrains.kotlin:kotlin-reflect:${rootProject.properties["kotlinReflectDependencyVersion"]}")
        implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:${rootProject.properties["kotlinStdlibJdk8DependencyVersion"]}")
        implementation("org.springframework.boot:spring-boot-starter:${rootProject.properties["springFrameworkVersion"]}")
        testImplementation("org.springframework.boot:spring-boot-starter-test:${rootProject.properties["springFrameworkVersion"]}") {
            exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
        }
    }

    tasks.test {
        useJUnitPlatform()
    }

    tasks.withType<KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjsr305=strict")
            jvmTarget = "11"
            apiVersion = "1.6"
            languageVersion = "1.6"
        }
    }
    tasks.withType<Test> {
        useJUnitPlatform()
        // after all tests generate aggregated test report
        finalizedBy(aggregatedTestReport)
        // after all tests generate aggregated test coverage report
        finalizedBy(aggregatedJacocoRootReport)

        testLogging {
            events("skipped", "passed", "failed")
        }

        // perform tests on single thread
        maxParallelForks = 1
        // in case of lack of memory create heap dump
        jvmArgs("-XX:+HeapDumpOnOutOfMemoryError")
    }
}
