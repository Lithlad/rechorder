package song.rest

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.lang.NonNull
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import song.parser.application.SongParserApplicationServiceImpl
import song.parser.domain.Song
import song.rest.form.ParseSongForm
import javax.annotation.PostConstruct

// TODO rest controller tests
@RestController
@RequestMapping("/api/song")
class SongController(private val songParserApplicationService: SongParserApplicationServiceImpl) {

    val logger: Logger = LoggerFactory.getLogger(javaClass)

    @PostConstruct
    fun PostConstructAction() {
        println("SongController created")
    }

    @PostMapping("/parse")
    fun parse(
        @NonNull @Validated @RequestBody
        form: ParseSongForm
    ): ResponseEntity<Song> {
        logger.info("Parse song called " + form)
        val parsedSong = songParserApplicationService.parseFromUltimateGuitarTabs(form, emptySet())
        return ResponseEntity.ok(parsedSong)
    }
}
