package song.rest.form

import song.parser.application.command.ParseSongCommand

data class ParseSongForm(
    private val title: String,
    private val author: String,
    private val album: String,
    private val year: Int,
    private val text: String
) : ParseSongCommand {
    override fun getTitle(): String = title
    override fun getAuthor(): String = author
    override fun getAlbum(): String = album
    override fun getYear(): Int = year
    override fun getText(): String = text
}
