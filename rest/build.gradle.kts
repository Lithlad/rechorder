dependencies {
    implementation(project(":core"))
    implementation("org.springframework.boot:spring-boot-starter-web:${rootProject.properties["springFrameworkVersion"]}")
}

description = "rest"
version = "0.0.1-SNAPSHOT"
group = "com.chaber"
