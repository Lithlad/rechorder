# Getting Started
Application is created for fun. Not everything is made as it ought to be.

### Must be installed
* NPM

# Troubleshooting
### ZipFile invalid LOC header (bad signature) vaadin
* ./gradlew clean
* Delete content of directory
** .gradle/caches, .gradle/native, .gradle/notifications, .gradle/.tmp

### Application start
* Build by `./gradlew clean build`
* Start application :)
### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.5.5/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.5.5/maven-plugin/reference/html/#build-image)
* [Spring Data MongoDB](https://docs.spring.io/spring-boot/docs/2.5.5/reference/htmlsingle/#boot-features-mongodb)
* [Vaadin](https://vaadin.com/spring)

### Guides
The following guides illustrate how to use some features concretely:

* [Accessing Data with MongoDB](https://spring.io/guides/gs/accessing-data-mongodb/)
* [Creating CRUD UI with Vaadin](https://spring.io/guides/gs/crud-with-vaadin/)

### Development tools
#### Gitlab
https://gitlab.com/Lithlad/rechorder

#### Sonar
https://sonarcloud.io/project/configuration?id=Lithlad_rechorder

### Todo
- suggest you to use NoSQL support data migration library such as mongock. It provide data migration most of time like liquibase. mongock provide migration class instead of liquibase XML or JSON.
- kubernetes introduction
- monitoring introduction 
  - spring actuator (https://www.baeldung.com/spring-boot-health-indicators)
  - Prometheus
  - Grafana