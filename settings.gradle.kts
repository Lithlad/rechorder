rootProject.name = "rechorder"
include("core")
include("rest")
include("latexrepository")
include("ui")
include("main")

// TODO add jacoco version
// Definition of plugin versions, that needs to be used in project
pluginManagement {
    // Versions are loaded from gradle.properties
    val springFrameworkVersion: String by settings
    val springDependencyManagementPluginVersion: String by settings
    val vaadinDependencyVersion: String by settings
    val jvmPluginVersion: String by settings
    val pluginSpringVersion: String by settings
    val pluginSonarqubeVersion: String by settings
    val pluginGradleGitPropertiesVersion: String by settings
    val pluginKotlinterVersion: String by settings
    plugins {
        id("org.springframework.boot") version springFrameworkVersion
        id("io.spring.dependency-management") version springDependencyManagementPluginVersion
        id("com.vaadin") version vaadinDependencyVersion
        id("org.sonarqube") version pluginSonarqubeVersion
        id("com.gorylenko.gradle-git-properties") version pluginGradleGitPropertiesVersion
        id("org.jmailen.kotlinter") version pluginKotlinterVersion
        kotlin("jvm") version jvmPluginVersion
        kotlin("plugin.spring") version pluginSpringVersion
    }
}
