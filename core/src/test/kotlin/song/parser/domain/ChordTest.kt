package song.parser.domain

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import org.springframework.boot.test.system.CapturedOutput
import org.springframework.boot.test.system.OutputCaptureExtension

@ExtendWith(OutputCaptureExtension::class)
class ChordTest {

    @ParameterizedTest
    @ValueSource(
        strings = [
            "Am",
            "B",
            "Cmaj7/C",
            "Dm",
            "H",
            "Bm"
        ]
    )
    fun shouldReturnTrueIfStringIsChord(maybeChord: String) {
        // when
        val isChord = Chord.isChord(maybeChord)

        // then
        assertThat(isChord).isTrue
    }

    @ParameterizedTest
    @ValueSource(
        strings = [
            "a",
            "Bmol",
            "aa",
            "dm"
        ]
    )
    fun shouldReturnFalseIfStringIsNotChord(maybeChord: String, output: CapturedOutput) {
        // when
        val isChord = Chord.isChord(maybeChord)

        // then
        assertThat(isChord).isFalse
        assertThat(output).contains("Supposed value $maybeChord is not a chord")
    }

    @Test
    fun shouldReturnMappedStringToChord() {
        // given
        val maybeChord = "Cmaj7/C"

        // when
        val chord = Chord.toChord(maybeChord)

        // then
        assertThat(chord).isEqualTo(Chord.CMAJ7_C_DUR)
    }

    @ParameterizedTest
    @ValueSource(
        strings = [
            "a",
            "Bmol",
            "aa",
            "dm"
        ]
    )
    fun shouldReturnEmptyValueIfStringIsNotChord(maybeChord: String) {
        // when
        val isChord = Chord.toChord(maybeChord)

        // then
        assertThat(isChord).isNull()
    }
}
