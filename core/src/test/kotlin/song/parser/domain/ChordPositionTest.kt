package song.parser.domain

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.catchThrowable
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import song.parser.domain.exception.ChordFromPatternWasNotMatchedException

internal class ChordPositionTest {
    companion object {
        const val BEFORE_ALL_INDEX = Int.MIN_VALUE
        const val AFTER_ALL_INDEX = Int.MAX_VALUE
    }

    @Test
    fun shouldMapLineOfChordsFailingCase1() {
        // given
        val line = "C    Em   D"
        val indexOfFirstLetterInLine = 0
        val indexOfLastLetterInLine = 100

        // when
        val mappedLineToChords = ChordPosition.mapLineToChords(line, indexOfFirstLetterInLine, indexOfLastLetterInLine)

        // then
        assertThat(mappedLineToChords).isEqualTo(
            listOf(
                ChordPosition(Chord.C_DUR, 1),
                ChordPosition(Chord.E_MOL, 6),
                ChordPosition(Chord.D_DUR, 11)
            )
        )
    }

    @Test
    fun shouldMapLineOfChordsFailingCase2() {
        // given
        val line = "Bm     F#m   Bm     F#m"
        val indexOfFirstLetterInLine = 0
        val indexOfLastLetterInLine = 100

        // when
        val mappedLineToChords = ChordPosition.mapLineToChords(line, indexOfFirstLetterInLine, indexOfLastLetterInLine)

        // then
        assertThat(mappedLineToChords).isEqualTo(
            listOf(
                ChordPosition(Chord.B_MOL, 1),
                ChordPosition(Chord.FIS_MOL, 8),
                ChordPosition(Chord.B_MOL, 14),
                ChordPosition(Chord.FIS_MOL, 21)
            )
        )
    }

    @Test
    fun shouldMapLineOfChords() {
        // given
        val line = "Am             C        Gm         Em"
        val indexOfFirstLetterInLine = 0
        val indexOfLastLetterInLine = 100

        // when
        val mappedLineToChords = ChordPosition.mapLineToChords(line, indexOfFirstLetterInLine, indexOfLastLetterInLine)

        // then
        assertThat(mappedLineToChords).isEqualTo(
            listOf(
                ChordPosition(Chord.A_MOL, 1),
                ChordPosition(Chord.C_DUR, 16),
                ChordPosition(Chord.G_MOL, 25),
                ChordPosition(Chord.E_MOL, 36)
            )
        )
    }

    @Test
    fun shouldMapLineOfChordsWithSpecialSignInTheMiddle() {
        // given
        val line = "Am             Cmaj7/C        Gm         Em"
        val indexOfFirstLetterInLine = 0
        val indexOfLastLetterInLine = 100

        // when
        val mappedLineToChords = ChordPosition.mapLineToChords(line, indexOfFirstLetterInLine, indexOfLastLetterInLine)

        // then
        assertThat(mappedLineToChords).isEqualTo(
            listOf(
                ChordPosition(Chord.A_MOL, 1),
                ChordPosition(Chord.CMAJ7_C_DUR, 16),
                ChordPosition(Chord.G_MOL, 31),
                ChordPosition(Chord.E_MOL, 42)
            )
        )
    }

    @Test
    fun shouldMapLineOfChordsWithSpecialSignOnTheBeginning() {
        // given
        val line = "Cmaj7/C             C        Gm         Em"
        val indexOfFirstLetterInLine = 0
        val indexOfLastLetterInLine = 100

        // when
        val mappedLineToChords = ChordPosition.mapLineToChords(line, indexOfFirstLetterInLine, indexOfLastLetterInLine)

        // then
        assertThat(mappedLineToChords).isEqualTo(
            listOf(
                ChordPosition(Chord.CMAJ7_C_DUR, 1),
                ChordPosition(Chord.C_DUR, 21),
                ChordPosition(Chord.G_MOL, 30),
                ChordPosition(Chord.E_MOL, 41)
            )
        )
    }

    @Test
    fun shouldMapLineOfChordsWhenLastChordExceedsVerseLength() {
        // given
        val line = "Am             C        Gm         Em"
        val indexOfFirstLetterInLine = 0
        val indexOfLastLetterInLine = 35

        // when
        val mappedLineToChords = ChordPosition.mapLineToChords(line, indexOfFirstLetterInLine, indexOfLastLetterInLine)

        // then
        assertThat(mappedLineToChords).isEqualTo(
            listOf(
                ChordPosition(Chord.A_MOL, 1),
                ChordPosition(Chord.C_DUR, 16),
                ChordPosition(Chord.G_MOL, 25),
                ChordPosition(Chord.E_MOL, AFTER_ALL_INDEX)
            )
        )
    }

    @Test
    fun shouldMapLineOfChordsWhenFirstChordsStartsBeforeVerse() {
        // given
        val line = "Am             C        Gm         Em"
        val indexOfFirstLetterInLine = 23
        val indexOfLastLetterInLine = 100

        // when
        val mappedLineToChords = ChordPosition.mapLineToChords(line, indexOfFirstLetterInLine, indexOfLastLetterInLine)

        // then
        assertThat(mappedLineToChords).isEqualTo(
            listOf(
                ChordPosition(Chord.A_MOL, BEFORE_ALL_INDEX),
                ChordPosition(Chord.C_DUR, BEFORE_ALL_INDEX),
                ChordPosition(Chord.G_MOL, 2),
                ChordPosition(Chord.E_MOL, 13)
            )
        )
    }

    @Test
    fun shouldMapLineOfChordsWhenThereIsOnlyOneChord() {
        // given
        val line = " D"
        val indexOfFirstLetterInLine = 1
        val indexOfLastLetterInLine = 100

        // when
        val mappedLineToChords = ChordPosition.mapLineToChords(line, indexOfFirstLetterInLine, indexOfLastLetterInLine)

        // then
        assertThat(mappedLineToChords).isEqualTo(
            listOf(
                ChordPosition(Chord.D_DUR, 1)
            )
        )
    }

    @ParameterizedTest
    @ValueSource(
        strings = [
            "Lost but now I am found\n",
            "                        We were born to die\n"
        ]
    )
    fun shouldNotMapText(line: String) {
        // given
        val indexOfFirstLetterInLine = 0
        val indexOfLastLetterInLine = 100

        // when
        val exception =
            catchThrowable { ChordPosition.mapLineToChords(line, indexOfFirstLetterInLine, indexOfLastLetterInLine) }

        // then
        assertThat(exception)
            .isInstanceOf(ChordFromPatternWasNotMatchedException::class.java)
    }

    @ParameterizedTest
    @ValueSource(
        strings = [
            "Am             C        Gm         Em",
            "Am             Cmaj7/C        Gm         Em",
            "Cmaj7/C             C        Gm         Em",
            "                        Cmaj7/C",
            " D",
            "C    Em   D"
        ]
    )
    fun shouldReturnTrueIfLineIsChords(line: String) {
        // when
        val isChords = ChordPosition.isLineOfChords(line)

        // then
        assertThat(isChords).isTrue
    }

    @Test
    fun shouldReturnFalseIfLineIsNotChords() {
        // given
        val line = "                        We were born to die\n"

        // when
        val isChords = ChordPosition.isLineOfChords(line)

        // then
        assertThat(isChords).isFalse
    }
}
