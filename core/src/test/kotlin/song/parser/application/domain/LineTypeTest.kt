package song.parser.application.domain

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

internal class LineTypeTest {

    @ParameterizedTest
    @ValueSource(
        strings = [
            "[Chorus]",
            "[Verse 1]",
            "[Bridge]",
            "[Outro]"
        ]
    )
    fun shouldReturnFunctionalLineType(line: String) {
        // when
        val isChords = LineType.resolveLineType(line)

        // then
        Assertions.assertThat(isChords).isEqualTo(LineType.FUNCTIONAL)
    }

    @Test
    fun shouldReturnChordsLineType() {
        // given
        val line = "Am             C        Gm         Em"

        // when
        val isChords = LineType.resolveLineType(line)

        // then
        Assertions.assertThat(isChords).isEqualTo(LineType.CHORDS)
    }

    @Test
    fun shouldReturnTextLineType() {
        // given
        val line = "                        We were born to die\n"

        // when
        val isChords = LineType.resolveLineType(line)

        // then
        Assertions.assertThat(isChords).isEqualTo(LineType.TEXT)
    }

    @Test
    fun shouldReturnNothingTypeLine() {
        // given
        val line = "                  		"

        // when
        val isChords = LineType.resolveLineType(line)

        // then
        Assertions.assertThat(isChords).isEqualTo(LineType.NOTHING)
    }
}
