package song.parser.application.domain

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import song.parser.ExpectedSong
import song.parser.TestSong
import song.parser.domain.Verse

internal class SongParserDomainServiceTest {
    private val songParserDomainService = SongParserDomainService()

    @Test
    fun shouldParseSong() {
        // given
        val text = TestSong.BASIC.getText()

        // when
        val parsedVerses: List<Verse> = songParserDomainService.parseFromUltimateGuitarTabs(text)

        // then
        val expectedVerses = ExpectedSong().getExpectedVerses()
        assertThat(parsedVerses).isEqualTo(expectedVerses)
    }

    @Test
    fun shouldParseSongWithoutErrors() {
        // given
        val text = TestSong.EXTENDED.getText()

        // when
        val parsedVerses: List<Verse> = songParserDomainService.parseFromUltimateGuitarTabs(text)

        // then
        assertThat(parsedVerses).isNotEmpty
    }
}
