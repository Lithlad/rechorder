package song.parser.application.domain

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

internal class FunctionalParameterTest {

    @Test
    fun shouldReturnTrueIfParameterIsIntro() {
        // given
        val line = "     [Intro]  	"

        // when
        val doesSuit = FunctionalParameter.INTRO.doesSuit(line)

        // then
        assertThat(doesSuit).isTrue
    }

    @Test
    fun shouldReturnFalseIfParameterIsNotIntro() {
        // given
        val line = "Intro"

        // when
        val doesSuit = FunctionalParameter.INTRO.doesSuit(line)

        // then
        assertThat(doesSuit).isFalse
    }

    @Test
    fun shouldReturnTrueIfParameterIsVerse() {
        // given
        val line = "     [Verse 43]  	"

        // when
        val doesSuit = FunctionalParameter.STANZA.doesSuit(line)

        // then
        assertThat(doesSuit).isTrue
    }

    @Test
    fun shouldReturnFalseIfParameterIsNotVerse() {
        // given
        val line = "Verse 43"

        // when
        val doesSuit = FunctionalParameter.STANZA.doesSuit(line)

        // then
        assertThat(doesSuit).isFalse
    }

    @Test
    fun shouldReturnTrueIfParameterIsChorus() {
        // given
        val line = "	[Chorus]	"

        // when
        val doesSuit = FunctionalParameter.CHORUS.doesSuit(line)

        // then
        assertThat(doesSuit).isTrue
    }

    @Test
    fun shouldReturnFalseIfParameterIsNotChorus() {
        // given
        val line = "Chorus"

        // when
        val doesSuit = FunctionalParameter.CHORUS.doesSuit(line)

        // then
        assertThat(doesSuit).isFalse
    }

    @Test
    fun shouldReturnTrueIfParameterIsBridge() {
        // given
        val line = "[Bridge]"

        // when
        val doesSuit = FunctionalParameter.BRIDGE.doesSuit(line)

        // then
        assertThat(doesSuit).isTrue
    }

    @Test
    fun shouldReturnFalseIfParameterIsNotBridge() {
        // given
        val line = "Bridge"

        // when
        val doesSuit = FunctionalParameter.BRIDGE.doesSuit(line)

        // then
        assertThat(doesSuit).isFalse
    }

    @Test
    fun shouldReturnTrueIfParameterIsOutro() {
        // given
        val line = "[Outro]"

        // when
        val doesSuit = FunctionalParameter.OUTRO.doesSuit(line)

        // then
        assertThat(doesSuit).isTrue
    }

    @Test
    fun shouldReturnFalseIfParameterIsNotOutro() {
        // given
        val line = "Outro"

        // when
        val doesSuit = FunctionalParameter.OUTRO.doesSuit(line)

        // then
        assertThat(doesSuit).isFalse
    }

    @Test
    fun shouldResolveFunctionalParameter() {
        // given
        val line = "[Chorus]"

        // when
        val resolveFunctionalParameter = FunctionalParameter.resolveFunctionalParameter(line)

        // then
        assertThat(resolveFunctionalParameter).isEqualTo(FunctionalParameter.CHORUS)
    }

    @Test
    fun shouldReturnEmptyIfFunctionalParameterIsNotMatched() {
        // given
        val line = "sdfasdf"

        // when
        val resolveFunctionalParameter = FunctionalParameter.resolveFunctionalParameter(line)

        // then
        assertThat(resolveFunctionalParameter).isNull()
    }

    @ParameterizedTest
    @ValueSource(
        strings = [
            "[Chorus]",
            "[Verse 1]",
            "[Bridge]",
            "[Outro]",
            "[Intro]"
        ]
    )
    fun shouldReturnTrueIfLineIsFunctionalParameter(line: String) {
        // when
        val isFunctionalParameter = FunctionalParameter.isFunctionalParameter(line)

        // then
        assertThat(isFunctionalParameter).isTrue
    }

    @Test
    fun shouldReturnFalseIfLineIsNotFunctionalParameter() {
        // given
        val line = "sdfasdf"

        // when
        val isFunctionalParameter = FunctionalParameter.isFunctionalParameter(line)

        // then
        assertThat(isFunctionalParameter).isFalse
    }
}
