package song.parser.application

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.anyString
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import song.parser.application.command.ParseSongCommand
import song.parser.application.domain.SongParserDomainService
import song.parser.domain.Song
import song.parser.domain.Verse

internal class SongParserApplicationServiceImplTest {
    companion object {
        private val ALBUM_NAME = "album name"
        private val AUTHOR = "author"
        private val TEXT = "text"
        private val TITLE = "title"
        private val YEAR = 1994
        private val VERSES = emptyList<Verse>()
    }

    private val songParserDomainService: SongParserDomainService = mock(SongParserDomainService::class.java)
    private val songParserApplicationService: SongParserApplicationService =
        SongParserApplicationServiceImpl(songParserDomainService)

    @BeforeEach
    internal fun setUp() {
        `when`(songParserDomainService.parseFromUltimateGuitarTabs(anyString())).thenReturn(VERSES)
    }

    @Test
    fun shouldParseFromUltimateGuitarTabs() {
        // given
        val command = mockParseCommand()
        val parser1 = mock(SongRepository::class.java)
        val parser2 = mock(SongRepository::class.java)
        val parsers = setOf(parser1, parser2)
        val expectedSong = Song("1", TITLE, AUTHOR, ALBUM_NAME, YEAR, VERSES)

        // when
        val obtainedSong = songParserApplicationService.parseFromUltimateGuitarTabs(command, parsers)

        // then
        verify(songParserDomainService).parseFromUltimateGuitarTabs(TEXT)
        verify(parser1).save(expectedSong)
        verify(parser2).save(expectedSong)
        assertThat(obtainedSong).isEqualTo(expectedSong)
    }

    private fun mockParseCommand(): ParseSongCommand {
        val parseSongCommand = mock(ParseSongCommand::class.java)
        `when`(parseSongCommand.getAlbum()).thenReturn(ALBUM_NAME)
        `when`(parseSongCommand.getAuthor()).thenReturn(AUTHOR)
        `when`(parseSongCommand.getText()).thenReturn(TEXT)
        `when`(parseSongCommand.getTitle()).thenReturn(TITLE)
        `when`(parseSongCommand.getYear()).thenReturn(YEAR)
        return parseSongCommand
    }
}
