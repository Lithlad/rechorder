package song.parser.application.exception

import song.parser.domain.exception.SongParserException

class UnrecognizedVerseType(classs: Class<*>) : SongParserException("Unrecognized verse type: '$classs'")
