package song.parser.application.command

interface ParseSongCommand {
    fun getTitle(): String
    fun getAuthor(): String
    fun getAlbum(): String
    fun getYear(): Int
    fun getText(): String
}
