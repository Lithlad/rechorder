package song.parser.application.domain

internal enum class FunctionalParameter {
    INTRO {
        override fun doesSuit(line: String): Boolean {
            val regex = Regex("""\[Intro\]""", setOf(RegexOption.IGNORE_CASE))
            return regex.containsMatchIn(line)
        }
    },
    STANZA {
        override fun doesSuit(line: String): Boolean {
            val regex = Regex("""\[Verse ?\d*\]""", setOf(RegexOption.IGNORE_CASE))
            return regex.containsMatchIn(line)
        }
    },
    CHORUS {
        override fun doesSuit(line: String): Boolean {
            val regex = Regex("""\[Chorus\]""", setOf(RegexOption.IGNORE_CASE))
            return regex.containsMatchIn(line)
        }
    },
    BRIDGE {
        override fun doesSuit(line: String): Boolean {
            val regex = Regex("""\[Bridge\]""", setOf(RegexOption.IGNORE_CASE))
            return regex.containsMatchIn(line)
        }
    },
    OUTRO {
        override fun doesSuit(line: String): Boolean {
            val regex = Regex("""\[Outro\]""", setOf(RegexOption.IGNORE_CASE))
            return regex.containsMatchIn(line)
        }
    };

    abstract fun doesSuit(line: String): Boolean

    companion object {
        fun resolveFunctionalParameter(line: String): FunctionalParameter? {
            return enumValues<FunctionalParameter>()
                .firstOrNull() { it.doesSuit(line) }
        }

        fun isFunctionalParameter(line: String): Boolean {
            return resolveFunctionalParameter(line) != null
        }
    }
}
