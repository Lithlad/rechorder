package song.parser.application.domain

import org.springframework.stereotype.Service
import song.parser.application.domain.exception.NonFunctionalLineWasNotExpectedException
import song.parser.application.domain.exception.SongParsingShouldBeAlreadyFinishedException
import song.parser.application.domain.exception.TextIsEmptyException
import song.parser.application.domain.exception.UnexpectedStateDuringTextWithChordsMergingException
import song.parser.domain.Bridge
import song.parser.domain.ChordPosition
import song.parser.domain.Chorus
import song.parser.domain.Intro
import song.parser.domain.Line
import song.parser.domain.Outro
import song.parser.domain.Stanza
import song.parser.domain.Verse
import song.parser.exception.SituationShouldNotHappenException

@Service
class SongParserDomainService {

    fun parseFromUltimateGuitarTabs(text: String): List<Verse> {
        val songWithoutEmptyLines = text.lines()
            .filter(this::lineIsNotEmpty)
        var currentLineIndex = 0
        val parsedVerses: MutableList<Verse> = mutableListOf()
        while (currentLineIndex < songWithoutEmptyLines.size) {
            val currentLine = retrieveCurrentLine(songWithoutEmptyLines, currentLineIndex)
            val newLineAndRecentlyAddedVerse: Pair<LineIndex, Verse> = when (
                val functionalParameter = FunctionalParameter.resolveFunctionalParameter(currentLine)
            ) {
                null -> throw NonFunctionalLineWasNotExpectedException(currentLine)
                else -> {
                    val (lineIndex, parsedLines) = parseSongVerse(
                        lineFromWhichVerseStarts(currentLineIndex),
                        songWithoutEmptyLines
                    )
                    when (functionalParameter) {
                        FunctionalParameter.INTRO -> Pair(lineIndex, Intro(parsedLines))
                        FunctionalParameter.STANZA -> Pair(lineIndex, Stanza(parsedLines))
                        FunctionalParameter.CHORUS -> Pair(lineIndex, Chorus(parsedLines))
                        FunctionalParameter.BRIDGE -> Pair(lineIndex, Bridge(parsedLines))
                        FunctionalParameter.OUTRO -> Pair(lineIndex, Outro(parsedLines))
                    }
                }
            }
            currentLineIndex = newLineAndRecentlyAddedVerse.first
            parsedVerses.add(newLineAndRecentlyAddedVerse.second)
        }
        return parsedVerses.toList()
    }

    private fun lineIsNotEmpty(line: String): Boolean {
        return line.trim().isNotEmpty()
    }

    private fun lineFromWhichVerseStarts(currentLineIndex: LineIndex): LineIndex {
        return currentLineIndex + 1
    }

    private fun parseSongVerse(
        startingLineIndex: LineIndex,
        songWithoutEmptyLines: List<String>
    ): CurrentLineIndexAndParsedVerse {
        var currentLineIndex = startingLineIndex
        val lines: MutableList<Line> = mutableListOf()
        while (currentLineIndex < songWithoutEmptyLines.size) {
            val currentLine = retrieveCurrentLine(songWithoutEmptyLines, currentLineIndex)
            val (nextLineIndex, newLines) = when (
                val functionalParameter =
                    FunctionalParameter.resolveFunctionalParameter(currentLine)
            ) {
                null -> joinChordsWithText(currentLineIndex, songWithoutEmptyLines)
                functionalParameter -> return CurrentLineIndexAndParsedVerse(currentLineIndex, lines.toList())
                else -> throw SituationShouldNotHappenException("Expected functional parameter or chords with text")
            }
            currentLineIndex = nextLineIndex
            lines.add(newLines)
        }
        // last verse
        return CurrentLineIndexAndParsedVerse(Int.MAX_VALUE, lines.toList())
    }

    // returns: index of next line to be parsed and currently parsed line
    private fun joinChordsWithText(
        currentLineIndex: LineIndex,
        songWithoutEmptyLines: List<String>
    ): CurrentLineIndexAndParsedLine {
        val expectedChords = retrieveCurrentLine(songWithoutEmptyLines, currentLineIndex)
        val expectedText = retrieveNextLine(songWithoutEmptyLines, currentLineIndex)

        return when (
            AdjacentLinesTypes(
                LineType.resolveLineType(expectedChords),
                LineType.resolveLineType(expectedText)
            )
        ) {
            AdjacentLinesTypes(LineType.CHORDS, LineType.CHORDS),
            AdjacentLinesTypes(LineType.CHORDS, LineType.NOTHING),
            AdjacentLinesTypes(LineType.CHORDS, LineType.FUNCTIONAL) ->
                CurrentLineIndexAndParsedLine(
                    currentLineIndex + 1,
                    Line(Line.EMPTY_TEXT, ChordPosition.mapLineToChordsWithoutText(expectedChords))
                )

            AdjacentLinesTypes(LineType.CHORDS, LineType.TEXT) -> {
                val (indexOfFirstLetterInLine, indexOfLastLetterInLine) = retrieveIndexOfFirstAndLastNonWhitespaceLetterInLine(
                    expectedText
                )
                CurrentLineIndexAndParsedLine(
                    currentLineIndex + 2,
                    Line(
                        expectedText.trim(),
                        ChordPosition.mapLineToChords(
                            expectedChords,
                            indexOfFirstLetterInLine,
                            indexOfLastLetterInLine
                        )
                    )
                )
            }

            AdjacentLinesTypes(LineType.TEXT, LineType.CHORDS) ->
                CurrentLineIndexAndParsedLine(currentLineIndex + 1, Line(expectedText.trim(), Line.EMPTY_CHORDS))

            AdjacentLinesTypes(LineType.TEXT, LineType.FUNCTIONAL) ->
                CurrentLineIndexAndParsedLine(currentLineIndex + 1, Line(expectedChords.trim(), Line.EMPTY_CHORDS))

            else -> throw UnexpectedStateDuringTextWithChordsMergingException(
                currentLineIndex,
                expectedChords,
                expectedText
            )
        }
    }

    private fun retrieveIndexOfFirstAndLastNonWhitespaceLetterInLine(text: String): IndexOfFirstAndLastNonWhitespaceLetterInLine {
        fun indexOfFirstNonWhitespaceLetterInLine(text: String): LineIndex {
            for (i in 0..text.length) {
                if (!text[i].isWhitespace()) return i
            }
            throw TextIsEmptyException()
        }

        fun indexOfLastNonWhitespaceLetterInLine(text: String): LineIndex {
            for (i in text.length - 1 downTo 0) {
                if (!text[i].isWhitespace()) return i
            }
            throw TextIsEmptyException()
        }
        return IndexOfFirstAndLastNonWhitespaceLetterInLine(
            indexOfFirstNonWhitespaceLetterInLine(text),
            indexOfLastNonWhitespaceLetterInLine(text)
        )
    }

    private fun retrieveCurrentLine(songWithoutEmptyLines: List<String>, currentLineIndex: LineIndex): String {
        if (currentLineIndex >= songWithoutEmptyLines.size) {
            throw SongParsingShouldBeAlreadyFinishedException()
        }
        return songWithoutEmptyLines[currentLineIndex]
    }

    private fun retrieveNextLine(songWithoutEmptyLines: List<String>, currentLineIndex: LineIndex): String {
        if (currentLineIndex + 1 >= songWithoutEmptyLines.size) {
            return Line.EMPTY_TEXT
        }
        return songWithoutEmptyLines[currentLineIndex + 1]
    }
}

private data class AdjacentLinesTypes(val firstLineType: LineType, val secondLineType: LineType)
private data class CurrentLineIndexAndParsedVerse(val currentLineIndex: LineIndex, val parsedLines: List<Line>)
private data class CurrentLineIndexAndParsedLine(val currentLineIndex: LineIndex, val parsedLine: Line)
private data class IndexOfFirstAndLastNonWhitespaceLetterInLine(
    val firstNonWhitespaceLetterInLineIndex: LineIndex,
    val LastNonWhitespaceLetterInLineIndex: LineIndex
)

private typealias LineIndex = Int
