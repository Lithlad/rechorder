package song.parser.application.domain.exception

import song.parser.domain.exception.SongParserException

class UnexpectedStateDuringTextWithChordsMergingException(
    lineIndex: Int,
    expectedChords: String,
    expectedText: String
) : SongParserException("Lines starting from $lineIndex contains not predicted state: $expectedChords, $expectedText")
