package song.parser.application.domain.exception

import song.parser.domain.exception.SongParserException

class NonFunctionalLineWasNotExpectedException(line: String) :
    SongParserException("Line with content '$line' was not expected")
