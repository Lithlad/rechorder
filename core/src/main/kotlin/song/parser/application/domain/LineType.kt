package song.parser.application.domain

import song.parser.domain.ChordPosition

internal enum class LineType {
    FUNCTIONAL, CHORDS, TEXT, NOTHING;

    companion object {
        fun resolveLineType(line: String): LineType {
            if (FunctionalParameter.isFunctionalParameter(line)) {
                return FUNCTIONAL
            }
            if (ChordPosition.isLineOfChords(line)) {
                return CHORDS
            }
            if (line.trim().isEmpty()) {
                return NOTHING
            }
            return TEXT
        }
    }
}
