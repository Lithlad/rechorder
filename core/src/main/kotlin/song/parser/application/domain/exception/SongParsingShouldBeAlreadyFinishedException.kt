package song.parser.application.domain.exception

import song.parser.domain.exception.SongParserException

class SongParsingShouldBeAlreadyFinishedException : SongParserException("Song parsing exceeded over song lines number")
