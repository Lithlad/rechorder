package song.parser.application.domain.exception

import song.parser.domain.exception.SongParserException

class TextIsEmptyException : SongParserException("In this place empty text is not expected")
