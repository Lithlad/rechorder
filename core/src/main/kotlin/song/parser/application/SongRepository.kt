package song.parser.application

import song.parser.application.exception.UnrecognizedVerseType
import song.parser.domain.Bridge
import song.parser.domain.Chorus
import song.parser.domain.Intro
import song.parser.domain.Outro
import song.parser.domain.Song
import song.parser.domain.Stanza

abstract class SongRepository<SONG, out I, out B, out C, out O, out S> {
    abstract fun getRepositoryName(): String

    abstract fun save(song: Song): SONG

    protected abstract fun parseIntro(intro: Intro): I

    protected abstract fun parseBridge(bridge: Bridge): B

    protected abstract fun parseChorus(chorus: Chorus): C

    protected abstract fun parseOutro(outro: Outro): O

    protected abstract fun parseStanza(stanza: Stanza): S

    protected fun mapSongVerses(song: Song) =
        song.verses.map { verse ->
            when (verse) {
                is Intro -> parseIntro(verse)
                is Stanza -> parseStanza(verse)
                is Chorus -> parseChorus(verse)
                is Bridge -> parseBridge(verse)
                is Outro -> parseOutro(verse)
                else -> throw UnrecognizedVerseType(verse.javaClass)
            }
        }
}
