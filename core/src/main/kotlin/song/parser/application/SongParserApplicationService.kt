package song.parser.application

import song.parser.application.command.ParseSongCommand
import song.parser.domain.Song

interface SongParserApplicationService {

    fun parseFromUltimateGuitarTabs(
        parseSongCommand: ParseSongCommand,
        songOutputParsers: Set<SongRepository<*, *, *, *, *, *>>
    ): Song
}
