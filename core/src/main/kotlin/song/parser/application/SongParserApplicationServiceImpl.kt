package song.parser.application

import org.springframework.stereotype.Service
import song.parser.application.command.ParseSongCommand
import song.parser.application.domain.SongParserDomainService
import song.parser.domain.Song
import javax.annotation.PostConstruct

@Service
class SongParserApplicationServiceImpl(
    private val songParserDomainService: SongParserDomainService
) : SongParserApplicationService {

    @PostConstruct
    fun PostConstructAction() {
        println("SongParserApplicationService created")
    }

    override fun parseFromUltimateGuitarTabs(
        parseSongCommand: ParseSongCommand,
        songOutputParsers: Set<SongRepository<*, *, *, *, *, *>>
    ): Song {
        val parsedText = songParserDomainService.parseFromUltimateGuitarTabs(parseSongCommand.getText())
        val song = Song(
            "1",
            parseSongCommand.getTitle(),
            parseSongCommand.getAuthor(),
            parseSongCommand.getAlbum(),
            parseSongCommand.getYear(),
            parsedText
        )
        songOutputParsers.forEach {
            it.save(song)
        }
        return song
    }
}
