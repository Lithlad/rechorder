package song.parser.domain

data class Song(
    val id: String,
    val title: String,
    val author: String,
    val album: String,
    val year: Int,
    val verses: List<Verse>
)
