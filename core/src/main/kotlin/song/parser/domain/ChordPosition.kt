package song.parser.domain

import song.parser.domain.Chord.Companion.toChord
import song.parser.domain.exception.ChordFromPatternWasNotMatchedException
import song.parser.exception.SituationShouldNotHappenException

data class ChordPosition(val chord: Chord, val position: Int? = null) {
    companion object {
        const val BEFORE_ALL_INDEX = Int.MIN_VALUE
        const val AFTER_ALL_INDEX = Int.MAX_VALUE
        private val CHORD_REGEX = Regex("""((\w|#|\/)+\s{1})|(\s{1}(\w|#|\/)+\s{1})|(\s{1}(\w|#|\/)+${'$'})""")

        fun mapLineToChords(
            lineWithExpectedChords: String,
            indexOfFirstLetterInLine: Int,
            indexOfLastLetterInLine: Int
        ): List<ChordPosition> {
            if (!isLineOfChords(lineWithExpectedChords)) throw ChordFromPatternWasNotMatchedException(
                lineWithExpectedChords
            )
            return CHORD_REGEX.findAll(lineWithExpectedChords).run {
                map { foundByRegexChord ->
                    val maybeMappedChord = toChord(foundByRegexChord.value)
                    if (maybeMappedChord != null) FoundChordPosition(
                        ChordPosition(
                            maybeMappedChord,
                            fixExactBeginningOfChordWithNummeringFromOne(foundByRegexChord)
                        )
                    )
                    else NonMatchedString(foundByRegexChord.value)
                }
                    .map {
                        when (it) {
                            is NonMatchedString -> throw ChordFromPatternWasNotMatchedException(it.stringChord)
                            is FoundChordPosition -> ChordPosition(
                                it.chordPosition.chord,
                                fixChordPosition(
                                    indexOfFirstLetterInLine,
                                    indexOfLastLetterInLine,
                                    it.chordPosition.position!!
                                )
                            )
                        }
                    }
                    .toList()
            }
        }

        private fun fixChordPosition(
            indexOfFirstLetterInVerse: Int,
            indexOfLastLetterInVerse: Int,
            chordPosition: Int
        ): Int {
            if (indexOfLastLetterInVerse < chordPosition) {
                return AFTER_ALL_INDEX
            }
            if (indexOfFirstLetterInVerse >= chordPosition) {
                return BEFORE_ALL_INDEX
            }
            return chordPosition - indexOfFirstLetterInVerse
        }

        fun mapLineToChordsWithoutText(line: String): List<ChordPosition> {
            return CHORD_REGEX.findAll(line).run {
                map { foundChord ->
                    val maybeMappedChord = enumValues<Chord>().firstOrNull() { it.doesFit(foundChord.value.trim()) }
                    val mappedChordOrNonMatchedString: Any = maybeMappedChord ?: foundChord.value
                    mappedChordOrNonMatchedString
                }
                    .map {
                        when (it) {
                            is String -> throw ChordFromPatternWasNotMatchedException(it)
                            is Chord -> ChordPosition(it)
                            else -> throw SituationShouldNotHappenException("Excpected functional parameter or chords with text")
                        }
                    }
                    .toList()
            }
        }

        fun isLineOfChords(line: String): Boolean {
            val maybeChords = line.split(" ")
                .filter { str -> str.isNotEmpty() }
                .map { maybeChord -> maybeChord.trim() }
            return maybeChords.all { maybeChord -> Chord.isChord(maybeChord) }
        }

        private fun fixExactBeginningOfChordWithNummeringFromOne(foundChord: MatchResult): Int {
            fun retrieveFixationFactorForChord(value: String): Int {
                val fixationFactorForChordNotStartingWithWhitespace = 1
                val fixationFactorForChordStartingWithWhitespace = 2
                return if (value[0].isWhitespace()) fixationFactorForChordStartingWithWhitespace else fixationFactorForChordNotStartingWithWhitespace
            }
            return foundChord.range.first + retrieveFixationFactorForChord(foundChord.value)
        }
    }
}

private sealed class Either<out L, out R>
private data class NonMatchedString<out String>(val stringChord: String) : Either<String, Nothing>()
private data class FoundChordPosition<out ChordPosition>(val chordPosition: ChordPosition) :
    Either<Nothing, ChordPosition>()
