package song.parser.domain

import org.slf4j.Logger
import org.slf4j.LoggerFactory

enum class Chord(
    private val ultimateGuitarConvention: String,
    val desiredGuitarConvention: String = ultimateGuitarConvention
) {
    A_DUR("A"), A_MOL("Am", "a"), A_DUR5("A5"), A_DUR7("A7"), A_MOL7("Am7", "a7"), A_MAJ7("Amaj7"),
    ASUS("Asus"),
    A_MOL_G_DUR("Am/G", "a/G"), A_MOL_FIS_DUR("Am/F#", "a/Fis"), A_MOL_E_MOL("Am/Em", "a/e"),
    AB_DUR("Ab", "Gis"),
    B_DUR("B", "H"), B_MOL("Bm", "h"), B_DUR7("B7", "H7"), B_MOL7("Bm7", "h7"),
    BSUS4("Bsus4"), B7SUS4("B7sus4"),
    B_MOL11_A_DUR("Bm11/A"),
    C_DUR("C"), C_MOL("Cm", "c"), CMAJ7_DUR("Cmaj7"), CMAJ7_C_DUR("Cmaj7/C"), C_DUR7("C7"),
    CIS_MOL("C#m", "cis"), CIS_DUR_O("C#o", "Ciso"),
    C_DUR_A_DUR("C/A"), C_DUR_B_DUR("C/B"), C_DUR_G_DUR("C/G"),
    CADD9("Cadd9"),
    D_DUR("D"), D_MOL("Dm", "d"), D_DUR5("D5"), D_MOL6("Dm6", "d6"), D_DUR7("D7"), D_MOL7("Dm7", "d7"), D9("D9"),
    D_DUR_A_DUR("D/A"),
    D_MOL_F_DUR("Dm/F", "d/F"), D_DUR_FIS_DUR("D/F#", "D/Fis"),
    DSUS4("Dsus4"), DSUS4_D("Dsus4/D"),
    E_DUR("E"), E_MOL("Em", "e"), E_DUR5("E5"), E_DUR7("E7"),
    EB_DUR("Eb", "Dis"),
    ESUS4_E("Esus4/E"),
    F_DUR("F"), F_MOL("Fm", "f"), FMAJ7("Fmaj7"), F_DUR_7("F7"),
    FIS_DUR("F#", "Fis"), FIS_MOL("F#m", "fis"), FIS_DUR5("F#5", "Fis5"), FIS_DUR7("F#7", "Fis7"),
    FIS_DUR_O("F#o", "Fiso"),
    G_DUR("G"), G_MOL("Gm", "g"), G_DUR7("G7"), G_DUR7_D_DUR("G7/D"),
    G_DUR_B_DUR("G/B"), G7SUS4("G7sus4"), G_DUR_FIS_DUR("G/F#", "G/Fis"),
    GIS_DUR("G#", "Gis"),
    GDIM7("Gdim7"),
    H_DUR("H", "B"), H_DUR_2("Bb", "B"), H_MOL("Hm", "b");

    companion object {
        val logger: Logger = LoggerFactory.getLogger(Chord::class.java)

        private val ultimateGuitarConventionChordsStrings =
            values().map { chord -> chord.ultimateGuitarConvention }.toSet()

        fun isChord(maybeChord: String): Boolean =
            ultimateGuitarConventionChordsStrings.contains(maybeChord).apply {
                if (!this) logger.debug("Supposed value $maybeChord is not a chord")
            }

        fun toChord(searchedChord: String): Chord? {
            return enumValues<Chord>().firstOrNull { it.doesFit(searchedChord.trim()) }
        }
    }

    fun doesFit(stringChord: String): Boolean {
        return ultimateGuitarConvention == stringChord
    }
}
