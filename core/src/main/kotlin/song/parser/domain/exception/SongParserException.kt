package song.parser.domain.exception

abstract class SongParserException(errorMessage: String) : Exception(errorMessage)
