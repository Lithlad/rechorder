package song.parser.domain.exception

class ChordFromPatternWasNotMatchedException(chord: String) :
    SongParserException("Chord from pattern was not matched $chord")
