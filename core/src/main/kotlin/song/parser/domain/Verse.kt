package song.parser.domain

open class Verse(private val verses: List<Line>) {
    fun getLines(): List<Line> = verses
}

data class Intro(val verses: List<Line>) : Verse(verses)
data class Stanza(val verses: List<Line>) : Verse(verses)
data class Chorus(val verses: List<Line>) : Verse(verses)
data class Bridge(val verses: List<Line>) : Verse(verses)
data class Outro(val verses: List<Line>) : Verse(verses)
