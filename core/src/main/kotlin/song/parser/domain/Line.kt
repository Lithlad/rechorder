package song.parser.domain

data class Line(val text: String, val chordsPositions: List<ChordPosition>) {
    companion object {
        const val EMPTY_TEXT = ""
        val EMPTY_CHORDS: List<ChordPosition> = emptyList()
    }
}
