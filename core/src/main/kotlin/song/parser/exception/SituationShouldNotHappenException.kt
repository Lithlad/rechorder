package song.parser.exception

import song.parser.domain.exception.SongParserException

class SituationShouldNotHappenException(additionalInfo: String) :
    SongParserException("Situation should not happen: $additionalInfo")
