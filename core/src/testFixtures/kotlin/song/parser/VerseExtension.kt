package song.parser

import song.parser.domain.Bridge
import song.parser.domain.ChordPosition
import song.parser.domain.Chorus
import song.parser.domain.Intro
import song.parser.domain.Line
import song.parser.domain.Outro
import song.parser.domain.Stanza

fun Intro(vararg lines: Line): Intro = Intro(lines.asList())
fun Stanza(vararg lines: Line): Stanza = Stanza(lines.asList())
fun Chorus(vararg lines: Line): Chorus = Chorus(lines.asList())
fun Bridge(vararg lines: Line): Bridge = Bridge(lines.asList())
fun Outro(vararg lines: Line): Outro = Outro(lines.asList())

fun Line(text: String, vararg chordsPositions: ChordPosition): Line = Line(text, chordsPositions.asList())
