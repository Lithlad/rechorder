package song.parser

import song.parser.domain.Chord
import song.parser.domain.ChordPosition
import song.parser.domain.ChordPosition.Companion.AFTER_ALL_INDEX
import song.parser.domain.ChordPosition.Companion.BEFORE_ALL_INDEX
import song.parser.domain.Line

class ExpectedSong {

    fun getExpectedVerses() = listOf(
        Intro(
            Line(
                "Jestem na dworze sam, deszcz pada mi na głowę",
                ChordPosition(Chord.A_MOL, BEFORE_ALL_INDEX),
                ChordPosition(Chord.D_MOL, 22)
            )
        ),
        Stanza(
            Line(
                "Feet don't fail me now",
                ChordPosition(Chord.G_DUR, 1),
                ChordPosition(Chord.D_DUR, 21)
            ),
            Line(
                "Take me to the finish line",
                ChordPosition(Chord.G_DUR, 1),
                ChordPosition(Chord.D_DUR, 23)
            ),
            Line("It's like I told you honey", emptyList())
        ),
        Chorus(
            Line(
                "Don't make me sad, don't make me cry",
                ChordPosition(Chord.C_DUR, 1),
                ChordPosition(Chord.E_MOL, 15),
                ChordPosition(Chord.D_DUR, 35)
            ),
            Line(
                "Sometimes love is not enough when the road gets tough",
                ChordPosition(Chord.CMAJ7_C_DUR, 25)
            )
        ),
        Stanza(
            Line(
                "Lost but now I am found",
                ChordPosition(Chord.G_DUR, 1),
                ChordPosition(Chord.D_DUR, 21)
            ),
            Line(
                "I can see but once I was blind",
                ChordPosition(Chord.G_DUR, 1),
                ChordPosition(Chord.D_DUR, 27)
            ),
            Line("All the answers, honey", emptyList())
        ),
        Chorus(
            Line(
                "Don't make me sad, don't make me cry",
                ChordPosition(Chord.C_DUR, 1),
                ChordPosition(Chord.E_MOL, 15),
                ChordPosition(Chord.D_DUR, 35)
            ),
            Line(
                "Sometimes love is not enough when the road gets tough",
                ChordPosition(Chord.CMAJ7_DUR, 25)
            )
        ),
        Bridge(
            Line(
                "We were born to die",
                ChordPosition(Chord.C_DUR, BEFORE_ALL_INDEX),
                ChordPosition(Chord.E_MOL, BEFORE_ALL_INDEX),
                ChordPosition(Chord.D_DUR, 18),
                ChordPosition(Chord.CMAJ7_DUR, AFTER_ALL_INDEX)
            ),
            Line(
                "We were born to die",
                ChordPosition(Chord.C_DUR, BEFORE_ALL_INDEX),
                ChordPosition(Chord.E_MOL, BEFORE_ALL_INDEX),
                ChordPosition(Chord.D_DUR, 18)
            )
        ),
        Chorus(
            Line(
                "Come and take a walk on the wild side",
                listOf(ChordPosition(Chord.C_DUR, 1), ChordPosition(Chord.E_MOL, AFTER_ALL_INDEX))
            ),
            Line("Let me kiss you hard in the pouring rain", listOf(ChordPosition(Chord.D_DUR, 39)))
        ),
        Stanza(
            Line(
                "Tak niewiele żądam",
                listOf(
                    ChordPosition(Chord.B_MOL, 1),
                    ChordPosition(Chord.FIS_MOL, 8),
                    ChordPosition(Chord.B_MOL, 14),
                    ChordPosition(Chord.FIS_MOL, 2147483647)
                )
            ),
            Line(
                "Tak niewiele znaczę",
                listOf(
                    ChordPosition(Chord.B_MOL, 1),
                    ChordPosition(Chord.FIS_MOL, 8),
                    ChordPosition(Chord.B_MOL, 14),
                    ChordPosition(Chord.FIS_MOL, 2147483647)
                )
            ),
            Line(
                "Tak niewiele widziałem",
                listOf(
                    ChordPosition(Chord.B_MOL, 1),
                    ChordPosition(Chord.FIS_MOL, 8),
                    ChordPosition(Chord.B_MOL, 14),
                    ChordPosition(Chord.FIS_MOL, 21)
                )
            ),
            Line(
                "Tak niewiele zobaczę",
                listOf(ChordPosition(Chord.B_MOL, 1), ChordPosition(Chord.FIS_MOL, 8), ChordPosition(Chord.G_DUR, 17))
            )
        ),
        Outro(
            Line(
                "",
                ChordPosition(Chord.C_DUR),
                ChordPosition(Chord.E_MOL),
                ChordPosition(Chord.G_DUR),
                ChordPosition(Chord.C_DUR)
            ),
            Line("", listOf(ChordPosition(Chord.C_DUR), ChordPosition(Chord.E_MOL), ChordPosition(Chord.D_DUR)))
        )
    )
}
