package song.parser

enum class TestSong(private val file: String) {
    BASIC("inputSong.txt"), EXTENDED("inputSongExtended.txt");

    fun getText() = ExpectedSong::class.java.getResource(file).readText()
}
