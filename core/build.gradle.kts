plugins {
    `java-test-fixtures`
}

dependencies {
//    This dependency should be unneeded
    implementation("org.springframework.boot:spring-boot-starter-data-mongodb:${rootProject.properties["springFrameworkVersion"]}")
    testImplementation("org.junit.jupiter:junit-jupiter:${rootProject.properties["junitJupiterDependencyVersion"]}")
    testImplementation("org.assertj:assertj-core:${rootProject.properties["assertJDependencyVersion"]}")
}

description = "core"
version = "0.0.1-SNAPSHOT"
group = "com.chaber"
