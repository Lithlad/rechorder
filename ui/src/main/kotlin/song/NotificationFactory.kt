package song

import com.vaadin.flow.component.Text
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.button.ButtonVariant
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.component.icon.Icon
import com.vaadin.flow.component.notification.Notification
import com.vaadin.flow.component.notification.NotificationVariant
import com.vaadin.flow.component.orderedlayout.FlexComponent
import com.vaadin.flow.component.orderedlayout.HorizontalLayout

class NotificationFactory private constructor() {
    companion object {
        fun createInformationNotification(text: String): Notification {
            return createNotification(text, NotificationVariant.LUMO_SUCCESS, ButtonVariant.LUMO_SUCCESS)
        }

        fun createErrorNotification(text: String): Notification {
            return createNotification(text, NotificationVariant.LUMO_ERROR, ButtonVariant.LUMO_ERROR)
        }

        private fun createNotification(
            text: String,
            notificationVariant: NotificationVariant,
            buttonVariant: ButtonVariant
        ): Notification {
            val notification = Notification()
            notification.addThemeVariants(notificationVariant)
            notification.position = Notification.Position.TOP_CENTER

            val textDiv = Div(Text(text))

            val closeButton = createButton(buttonVariant)
            closeButton.addClickListener { notification.close() }

            val layout = HorizontalLayout(textDiv, closeButton)
            layout.alignItems = FlexComponent.Alignment.CENTER

            notification.add(layout)
            return notification
        }

        private fun createButton(buttonVariant: ButtonVariant): Button {
            val closeButton = Button(Icon("lumo", "cross"))
            closeButton.addThemeVariants(buttonVariant)
            closeButton.element.setAttribute("aria-label", "Close")
            return closeButton
        }
    }
}
