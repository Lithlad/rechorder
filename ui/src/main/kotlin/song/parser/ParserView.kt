package song.parser

import MainView
import com.vaadin.flow.component.HasComponents
import com.vaadin.flow.component.Key
import com.vaadin.flow.component.Unit
import com.vaadin.flow.component.button.Button
import com.vaadin.flow.component.formlayout.FormLayout
import com.vaadin.flow.component.listbox.MultiSelectListBox
import com.vaadin.flow.component.textfield.TextArea
import com.vaadin.flow.component.textfield.TextField
import com.vaadin.flow.data.binder.Binder
import com.vaadin.flow.data.converter.StringToIntegerConverter
import com.vaadin.flow.router.PageTitle
import com.vaadin.flow.router.Route
import com.vaadin.flow.router.RouteAlias
import song.NotificationFactory
import song.parser.application.SongParserApplicationService
import song.parser.application.SongRepository
import song.parser.form.ParserForm
import song.parser.validator.YearValidator

@Route(value = "parser", layout = MainView::class)
@RouteAlias(value = "", layout = MainView::class)
@PageTitle("Parser")
class ParserView(
    private var songParserApplicationService: SongParserApplicationService,
    private var songOutputParsers: Set<SongRepository<*, *, *, *, *, *>>
) : FormLayout(), HasComponents {

    private val titleField = TextField()
    private val authorField = TextField()
    private val albumField = TextField()
    private val yearField = TextField()
    private val textField = TextArea()
    private val outputDestination = MultiSelectListBox<String>()
    private val parseButton = Button("Parse")

    init {
        // TODO add internationalization
        val binder = Binder(ParserForm::class.java)
        val parserForm = ParserForm()

        addTitle(binder)
        addAuthor(binder)
        addAlbum(binder)
        addYear(binder)
        addDestination()
        addText(binder)
        addAcceptanceButton(binder, parserForm)
    }

    private fun addTitle(binder: Binder<ParserForm>) {
        titleField.setSizeFull()
        addFormItem(titleField, "Title")
        binder.forField(titleField)
            .asRequired("Song title is required")
            .bind("title")
    }

    private fun addAuthor(binder: Binder<ParserForm>) {
        authorField.setSizeFull()
        addFormItem(authorField, "Author")
        binder.forField(authorField)
            .asRequired("Song author is required")
            .bind("author")
    }

    private fun addAlbum(binder: Binder<ParserForm>) {
        albumField.setSizeFull()
        addFormItem(albumField, "Album")
        binder.forField(albumField)
            .bind("album")
    }

    private fun addYear(binder: Binder<ParserForm>) {
        addFormItem(yearField, "Year")
        binder.forField(yearField)
            .withValidator(YearValidator())
            .withConverter(StringToIntegerConverter("Year can not be mapped"))
            .bind("year")
    }

    private fun addDestination() {
        addFormItem(outputDestination, "Destination")
        outputDestination.setItems(songOutputParsers.map { it.getRepositoryName() })
    }

    private fun addText(binder: Binder<ParserForm>) {
        textField.setSizeFull()
        textField.setMinHeight(500.0F, Unit.PIXELS)
        val formItem = addFormItem(textField, "Ultimate guitar text")
        setColspan(formItem, 2)
        binder.forField(textField)
            .asRequired("Song text is required")
            .bind("text")
    }

    private fun addAcceptanceButton(binder: Binder<ParserForm>, parserForm: ParserForm) {
        parseButton.addClickListener {
            binder.writeBean(parserForm)
            parseSong(parserForm)
            NotificationFactory.createInformationNotification("Song was parsed successfully").open()
        }
        parseButton.addClickShortcut(Key.ENTER)
        addFormItem(parseButton, "")
    }

    private fun parseSong(parserForm: ParserForm) {
        val selectedItems = outputDestination.selectedItems
        val selectedParserOutputs = songOutputParsers.filter { selectedItems.contains(it.getRepositoryName()) }.toSet()
        songParserApplicationService.parseFromUltimateGuitarTabs(parserForm, selectedParserOutputs)
    }
}
