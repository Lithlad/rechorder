package song.parser.validator

import com.vaadin.flow.data.binder.ValidationResult
import com.vaadin.flow.data.binder.Validator
import com.vaadin.flow.data.binder.ValueContext

class YearValidator : Validator<String> {
    companion object {
        val yearRegex = Regex("""\d{4}""")
    }

    override fun apply(value: String?, context: ValueContext?): ValidationResult {
        if (value.isNullOrEmpty() || yearRegex.matches(value)) return ValidationResult.ok()
        return ValidationResult.error("Year value doesn't match requirements")
    }
}
