package song.parser.form

import song.parser.application.command.ParseSongCommand

data class ParserForm(
    private var title: String = "",
    private var author: String = "",
    private var album: String = "",
    private var year: Int = 0,
    private var text: String = ""
) : ParseSongCommand {

    override fun getTitle(): String = title
    override fun getAuthor(): String = author
    override fun getAlbum(): String = album
    override fun getYear(): Int = year
    override fun getText(): String = text

    fun setTitle(title: String) = run { this.title = title }
    fun setAuthor(author: String) = run { this.author = author }
    fun setAlbum(album: String) = run { this.album = album }
    fun setYear(year: Int) = run { this.year = year }
    fun setText(text: String) = run { this.text = text }
}
