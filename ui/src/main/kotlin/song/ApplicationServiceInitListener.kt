package song

import com.vaadin.flow.server.ServiceInitEvent
import com.vaadin.flow.server.SessionInitEvent
import com.vaadin.flow.server.SessionInitListener
import com.vaadin.flow.server.VaadinServiceInitListener
import org.springframework.stereotype.Component

@Component
class ApplicationServiceInitListener : VaadinServiceInitListener, SessionInitListener {
    override fun serviceInit(event: ServiceInitEvent?) {
        event?.source?.addSessionInitListener(this)
    }

    override fun sessionInit(event: SessionInitEvent?) {
        event?.session?.errorHandler = ExceptionHandler()
    }
}
