package song

import com.vaadin.flow.component.UI
import com.vaadin.flow.server.ErrorEvent
import com.vaadin.flow.server.ErrorHandler
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class ExceptionHandler : ErrorHandler {

    val logger: Logger = LoggerFactory.getLogger(javaClass)

    override fun error(error: ErrorEvent?) {
        logger.error(error.toString(), error?.throwable)
        if (UI.getCurrent() != null) {
            UI.getCurrent().access {
                val notification = NotificationFactory.createErrorNotification(error!!.throwable!!.message!!)
                notification.open()
            }
        }
    }
}
