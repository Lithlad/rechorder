import com.vaadin.flow.component.HasElement
import com.vaadin.flow.component.applayout.AppLayout
import com.vaadin.flow.component.html.Div
import com.vaadin.flow.component.html.Paragraph
import com.vaadin.flow.component.tabs.Tab
import com.vaadin.flow.component.tabs.Tabs
import com.vaadin.flow.router.RouterLayout

class MainView() : AppLayout(), RouterLayout {

    var childWrapper = Div()

    init {
        addToNavbar(Paragraph("Navbar"))
        addToDrawer(createDrawer())
        content = childWrapper
    }

    private fun createDrawer(): Tabs {
        val tabs = Tabs(Tab("Home"))
        tabs.orientation = Tabs.Orientation.VERTICAL
        return tabs
    }

    override fun showRouterLayoutContent(content: HasElement?) {
        childWrapper.element.appendChild(content?.element)
    }
}
