package song.parser.validator

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class YearValidatorTest {

    @Test
    fun shouldAcceptNullString() {
        // given
        val maybeYear = null

        // when
        val validationResult = YearValidator().apply(maybeYear, null)

        // then
        assertThat(validationResult.isError).isFalse
    }

    @Test
    fun shouldAcceptYear() {
        // given
        val maybeYear = "1994"

        // when
        val validationResult = YearValidator().apply(maybeYear, null)

        // then
        assertThat(validationResult.isError).isFalse
    }

    @Test
    fun shouldNotAcceptToShortYear() {
        // given
        val maybeYear = "194"

        // when
        val validationResult = YearValidator().apply(maybeYear, null)

        // then
        assertThat(validationResult.isError).isTrue
    }

    @Test
    fun shouldNotAcceptToLongYear() {
        // given
        val maybeYear = "19444"

        // when
        val validationResult = YearValidator().apply(maybeYear, null)

        // then
        assertThat(validationResult.isError).isTrue
    }

    @Test
    fun shouldAcceptOnlyDigits() {
        // given
        val maybeYear = "19a4"

        // when
        val validationResult = YearValidator().apply(maybeYear, null)

        // then
        assertThat(validationResult.isError).isTrue
    }
}
