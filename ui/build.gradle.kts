plugins {
    id("com.vaadin")
}

// Not deleted to remember about possibility - extra
// extra["vaadinVersion"] = "23.2.7"

vaadin {
    productionMode = true
}

dependencies {
    implementation(project(":core"))
    implementation("com.vaadin:vaadin-spring-boot-starter:${rootProject.properties["vaadinDependencyVersion"]}")
}

dependencyManagement {
    imports {
//        Not deleted to remember about possibility - extra
//        mavenBom("com.vaadin:vaadin-bom:${property("vaadinVersion")}")
        mavenBom("com.vaadin:vaadin-bom:${rootProject.properties["vaadinDependencyVersion"]}")
    }
}

description = "ui"
version = "0.0.1-SNAPSHOT"
group = "com.chaber"
