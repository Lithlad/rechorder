package song.repository.latex

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mockito.Mockito.verify
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.annotation.Import
import org.thymeleaf.TemplateEngine
import song.DummyLatexApplication
import song.configuration.TemplateEngineConfiguration
import song.parser.ExpectedSong
import song.parser.domain.Song

@SpringBootTest(classes = [DummyLatexApplication::class])
@Import(value = [SongLatexRepository::class, TemplateEngineConfiguration::class, TemplateEngine::class])
internal class SongLatexRepositoryTest {

    @Autowired
    private lateinit var songLatexRepository: SongLatexRepository

    @MockBean
    private lateinit var fileSaverRepository: FileSaverRepository

    @Test
    fun `should parse song`() {
        // given
        val expectedSong = SongLatexRepositoryTest::class.java.getResource("latexSong.tex").readText()
        val title = "Born to Die"
        val author = "Lana Del Rey"

        // when
        val generatedLatexFile = songLatexRepository.save(
            Song("", title, author, "Born to Die", 2012, ExpectedSong().getExpectedVerses())
        )

        // then
        verify(fileSaverRepository).save(title, author, expectedSong)
        assertThat(expectedSong).isEqualToIgnoringNewLines(generatedLatexFile)
    }
}
