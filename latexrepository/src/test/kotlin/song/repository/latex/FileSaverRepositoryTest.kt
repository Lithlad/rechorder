package song.repository.latex

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.assertj.core.api.AbstractStringAssert
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File
import java.nio.file.Files
import java.nio.file.Path

internal class FileSaverRepositoryTest {

    @TempDir
    lateinit var outputDirectory: File

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `should save song to file`() {
        // given
        val fileSaverRepository = FileSaverRepository(outputDirectory.absolutePath)
        val tile = "Laski z malymi cyckami"
        val author = "Brudne dzieci sida"
        val songLyrics = "song text"

        // when
        runTest { fileSaverRepository.save(tile, author, songLyrics) }

        // then
        assertThatFileContainsSongText("BrudneDzieciSida_LaskiZMalymiCyckami.sbd", songLyrics)
    }

    private fun assertThatFileContainsSongText(path: String, songLyrics: String): AbstractStringAssert<*> {
        fun ifPhotoFileExists(): Boolean =
            Files.exists(Path.of("$outputDirectory/$path"))

        fun readFileContent(): String =
            Files.readString(Path.of("$outputDirectory/$path"))

        assertThat(ifPhotoFileExists()).isTrue
        return assertThat(readFileContent()).isEqualTo(songLyrics)
    }
}
