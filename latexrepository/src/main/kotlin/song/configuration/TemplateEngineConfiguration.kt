package song.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.thymeleaf.TemplateEngine
import org.thymeleaf.spring5.SpringTemplateEngine
import org.thymeleaf.templatemode.TemplateMode
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver
import org.thymeleaf.templateresolver.ITemplateResolver

// Configuration classes can not be final, because spring creates additional proxy for them
@Configuration
open class TemplateEngineConfiguration {

    // Configuration methods can not be final, because spring creates additional proxy for them
    @Bean(name = ["latexTemplateEngine"])
    open fun textTemplateEngine(): TemplateEngine =
        SpringTemplateEngine().apply {
            addTemplateResolver(textTemplateResolver())
        }

    private fun textTemplateResolver(): ITemplateResolver =
        ClassLoaderTemplateResolver().apply {
            prefix = "/latex/views/"
            suffix = ".tex"
            templateMode = TemplateMode.TEXT
            characterEncoding = "UTF8"
            checkExistence = true
            isCacheable = true
        }
}
