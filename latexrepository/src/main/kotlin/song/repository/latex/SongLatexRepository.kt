package song.repository.latex

import kotlinx.coroutines.DelicateCoroutinesApi
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Repository
import org.thymeleaf.TemplateEngine
import org.thymeleaf.context.Context
import song.parser.application.SongRepository
import song.parser.domain.Bridge
import song.parser.domain.Chord
import song.parser.domain.Chorus
import song.parser.domain.Intro
import song.parser.domain.Outro
import song.parser.domain.Song
import song.parser.domain.Stanza
import song.parser.domain.Verse

// TODO class is open because of SongLatexRepositoryTest - make class final
@Repository
open class SongLatexRepository(
    @Qualifier("latexTemplateEngine")
    private val latexTemplateEngine: TemplateEngine,
    private val fileSaverRepository: FileSaverRepository
) : SongRepository<String, TexVerse, TexVerse, TexVerse, TexVerse, TexVerse>() {
    override fun getRepositoryName(): String = "Latex"

    @OptIn(DelicateCoroutinesApi::class)
    override fun save(song: Song): String {
        val context = Context().apply {
            setVariable("title", song.title)
            setVariable("author", song.author)
            setVariable("album", song.album)
            setVariable("year", song.year)
            setVariable("verses", mapSongVerses(song))
        }

        val latexSong = latexTemplateEngine.process("song", context)
        fileSaverRepository.save(song.title, song.author, latexSong!!)
        return latexSong
    }

    override fun parseIntro(intro: Intro): TexVerse = TexVerse(parseVerse(intro), LatexVerse.VERSE)

    override fun parseBridge(bridge: Bridge): TexVerse {
        val texLines = bridge.getLines().map { line ->
            line.chordsPositions.fold("") { acc, chord -> acc + " " + mapToLatexChord(chord.chord) }
        }
        return TexVerse(texLines, LatexVerse.VERSE)
    }

    override fun parseChorus(chorus: Chorus): TexVerse = TexVerse(parseVerse(chorus), LatexVerse.CHORUS)

    override fun parseOutro(outro: Outro): TexVerse = TexVerse(parseVerse(outro), LatexVerse.VERSE)

    override fun parseStanza(stanza: Stanza): TexVerse = TexVerse(parseVerse(stanza), LatexVerse.VERSE)

    private fun parseVerse(stanza: Verse): List<String> =
        stanza.getLines().map { singleVerse ->
            singleVerse.chordsPositions
                .foldRight(
                    StringBuilder(singleVerse.text)
                ) { chord, acc ->
                    acc.insert(
                        mapToLatexPosition(chord.position, acc.length),
                        mapToLatexChord(chord.chord)
                    )
                }
                .toString()
        }

    private fun mapToLatexPosition(chordPosition: Int?, lineLength: Int): Int =
        if (chordPosition == null || chordPosition > lineLength) lineLength
        else if (chordPosition < 0) 0
        else chordPosition

    private fun mapToLatexChord(chord: Chord): String = "\\[${chord.desiredGuitarConvention}]"
}
