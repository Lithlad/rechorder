package song.repository.latex

import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.io.File

// TODO should be replaced by @Repository . With @Repository
// - for some reason class can not be final because of aspects
// - inject values is not visible in internal methods
@Component
class FileSaverRepository(
    @Value("\${output.directory}") val outputDirectory: String
) {
    @OptIn(DelicateCoroutinesApi::class)
    internal fun save(title: String, author: String, latexSong: String) {
        GlobalScope.launch(context = Dispatchers.IO) {
            val file = File(outputDirectory, generateSongFileName(title, author))
            file.writeText(latexSong)
        }
    }

    private fun generateSongFileName(title: String, author: String): String {
        fun uppercaseFirstLetters(text: String): String =
            text.split(" ")
                .map { word -> word.trim() }
                .map { word -> word.replaceFirstChar { it.uppercase() } }
                .reduce { fullText, singleWord -> fullText + singleWord }

        return "${uppercaseFirstLetters(author)}_${uppercaseFirstLetters(title)}.sbd"
    }
}
