package song.repository.latex

data class TexVerse(val lines: List<String>, val latexVerse: LatexVerse)

enum class LatexVerse(val suffix: String) {
    CHORUS("chorus"), VERSE("verse")
}
