package song

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.ComponentScan

// TODO delete this class
// hack to provide SpringBootTest configuration. Don't start this application
@ComponentScan(basePackages = ["song"])
@SpringBootApplication
open class DummyLatexApplication
