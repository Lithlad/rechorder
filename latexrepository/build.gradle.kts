description = "latexrepository"
version = "0.0.1-SNAPSHOT"
group = "com.chaber"

plugins {
    `java-test-fixtures`
}

dependencies {
    implementation(project(":core"))
    implementation("org.mapstruct:mapstruct:${rootProject.properties["mapstructDependencyVersion"]}")
    implementation("org.springframework.boot:spring-boot-starter-thymeleaf:${rootProject.properties["springFrameworkVersion"]}")
    implementation("ognl:ognl:${rootProject.properties["ognlDependencyVersion"]}")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${rootProject.properties["kotlinxCoroutinesVersion"]}")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:${rootProject.properties["kotlinxCoroutinesVersion"]}")
    testFixturesApi(testFixtures(project(":core")))
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs += listOf(
            "-Xopt-in=kotlin.RequiresOptIn"
        )
    }
}
