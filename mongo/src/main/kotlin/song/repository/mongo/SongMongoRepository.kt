package song.repository.mongo

import org.mapstruct.factory.Mappers
import song.parser.application.SongRepository
import song.parser.domain.*
import org.springframework.stereotype.Repository
import song.repository.mongo.entity.SongMapper

@Repository
class SongMongoRepository(
	val songRepositoryMongoImplementation: SongRepositoryMongoImplementation): SongRepository<Song, Bridge, Chorus, Outro, Stanza>() {
	override fun save(song: Song): Song {
		val mapper = Mappers.getMapper(SongMapper::class.java)
		val songToEntity = mapper.songToEntity(song)
		return mapper.entityToSong(songRepositoryMongoImplementation.save(songToEntity))
	}

	override fun parseBridge(bridge: Bridge): Bridge {
		return bridge
	}

	override fun parseChorus(chorus: Chorus): Chorus {
		return chorus
	}

	override fun parseOutro(outro: Outro): Outro {
		return outro
	}

	override fun parseStanza(stanza: Stanza): Stanza {
		return stanza
	}

	override fun getRepositoryName(): String {
		return "Mongo repository"
	}
}
