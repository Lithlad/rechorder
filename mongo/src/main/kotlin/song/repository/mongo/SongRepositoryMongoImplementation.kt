package song.repository.mongo

import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository
import song.repository.mongo.entity.SongEntity

@Repository
interface SongRepositoryMongoImplementation : MongoRepository<SongEntity, String> {
}

