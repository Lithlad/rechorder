package song.repository.mongo.entity

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import song.parser.domain.Verse

@Document
data class SongEntity(
	@Id
	val id: String,
	val title: String,
	val author: String,
	val verses: List<Verse>) {
}