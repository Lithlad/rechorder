package song.repository.mongo.entity

import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import song.parser.domain.Song

@Mapper
interface SongMapper{

	@Mappings(
		Mapping(source = "id", target = "id"),
		Mapping(source = "title", target = "title"),
		Mapping(source = "author", target = "author"),
		Mapping(source = "verses", target = "verses")
	)
	fun songToEntity(song: Song) : SongEntity

	@InheritInverseConfiguration
	fun entityToSong(song: SongEntity) : Song
}