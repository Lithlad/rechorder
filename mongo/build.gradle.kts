dependencies {
    implementation(project(":core"))
    implementation("org.springframework.boot:spring-boot-starter-data-mongodb:${rootProject.properties["springFrameworkVersion"]}")
    implementation("org.mapstruct:mapstruct:${rootProject.properties["mapstructDependencyVersion"]}")
    testImplementation("de.flapdoodle.embed:de.flapdoodle.embed.mongo:3.5.2")
}

description = "mongo"
version = "0.0.1-SNAPSHOT"
group = "com.chaber"
